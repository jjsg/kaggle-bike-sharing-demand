import time
import math

from datetime import date
from math import log,sqrt,exp


def splitDatetime(data):
    data['date'], data['time'] = zip(*data['datetime'].apply(lambda x: x.split(' ', 1)))
    data['year'], data['month'], data['day'] = zip(*data['date'].apply(lambda x: x.split('-', 2)))
    data['hour'], data['minute'], data['second'] = zip(*data['time'].apply(lambda x: x.split(':', 2)))
    del data['time']
    temp={}
    temp['weekday']=[]
    for i in data['date']:
    	ref=i.split("-")
	d=date(int(ref[0]),int(ref[1]),int(ref[2]))
	temp['weekday'].append(d.isoweekday())
     
    data['weekday']=temp['weekday']
    del data['date']
    del data['minute']
    del data['second']
    del data['day']
    return data

    
def convertSeasonToBinary(data):
	temp={}
	temp['season1']=[]
	temp['season2']=[]
	temp['season3']=[]
	temp['season4']=[]
	for i in data['season']:
		ref=i
		if  ref==1 :
			temp['season1'].append(1)
			temp['season2'].append(0)
			temp['season3'].append(0)
			temp['season4'].append(0)
		elif ref==2 :
			temp['season1'].append(0)
			temp['season2'].append(1)
			temp['season3'].append(0)
			temp['season4'].append(0)
		elif ref==3 :
			temp['season1'].append(0)
			temp['season2'].append(0)
			temp['season3'].append(1)
			temp['season4'].append(0)
		elif ref==4 :
			temp['season1'].append(0)
			temp['season2'].append(0)
			temp['season3'].append(0)
			temp['season4'].append(1)
		else :
			temp['season1'].append(0)
			temp['season2'].append(0)
			temp['season3'].append(0)
			temp['season4'].append(0)

	data['season1']=temp['season1']
	data['season2']=temp['season2']
	data['season3']=temp['season3']
	data['season4']=temp['season4']

	del data['season']
	return data

def addWeekendFeature(data):
	temp={}
	temp['weekend']=[]
	temp['temp']=[]
	for i in range(len(data['holiday'])):
		r2=data['workingday'][i]
		temp['temp'].append(data['temp'][i]+data['atemp'][i])
		if  r2==0:
			temp['weekend'].append(1)
		else :
			temp['weekend'].append(0)
	data['holiday']=temp['weekend']
	
	return data

def countModification(data):
	temp={}
	temp['casual']=[]  
	temp['registered']=[]
	for i in range(len(data['casual'])):
		temp['casual'].append(log(1+int(data['casual'][i])))
		temp['registered'].append(log(1+int(data['registered'][i])))
	data['casual']=temp['casual']
	data['registered']=temp['registered']
	return data 
	 

