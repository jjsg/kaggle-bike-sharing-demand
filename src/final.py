dataPath = "../data/"
outPath = "../result/"

import pandas as pd
import functions,findScore
import math

from math import log,sqrt,exp
from sklearn.ensemble import RandomForestRegressor
from sklearn.ensemble import ExtraTreesRegressor
from functions import splitDatetime,convertSeasonToBinary,addWeekendFeature,countModification
from findScore import findScore

def loadData(datafile):
    return pd.read_csv(datafile)


def casualPredict(train,test,features,target):
	    est = RandomForestRegressor(n_estimators=700)
	    est.fit(train[features],train[target])
	    casual=est.predict(test[features])
	    temp=[]	
	    for i in range(len(casual)):
		temp.append(exp(casual[i])-1)
	    return temp		

def registeredPredict(train,test,features,target):
	    est = ExtraTreesRegressor(n_estimators=700)
	    est.fit(train[features],train[target])
	    casual=est.predict(test[features])
	    temp=[]	
	    for i in range(len(casual)):
		temp.append(exp(casual[i])-1)
	    return temp		


def main():

    train = loadData(dataPath + "train.csv")
    test = loadData(dataPath + "test.csv")
    train_test=loadData(dataPath+"submission_test.csv")

    train = splitDatetime(train)
    train = convertSeasonToBinary(train)
    train = addWeekendFeature(train)
    train = countModification(train)	
    test = splitDatetime(test)
    test = convertSeasonToBinary(test)	
    test = addWeekendFeature(test)	
    train_test=splitDatetime(train_test)	

    features = [col for col in train.columns if col not in ['datetime', 'casual', 'registered', 'count']]

    target='casual'
    casual=casualPredict(train,test,features,target)
   
    
    target='registered'	
    registered=registeredPredict(train,test,features,target)
    
    combined=[]
    for i in range(len(casual)):
	 temp=casual[i]+registered[i]
	 if temp<0:
         	combined.append(0)
	 else:
		combined.append(temp)

    print findScore(combined,train_test['count'])
    	 
    with open(outPath + "submission-RandomForest.csv", 'wb') as f:
        f.write("datetime,count\n")

        for i in range(len(combined)):
            f.write("%s,%s\n" % (test['datetime'][i],int(combined[i])))



if __name__ == "__main__":
    main()
	
