import math

from math import log,sqrt,exp


def findScore(predicted,actual):
	res=0
	for i in range(len(predicted)):
		lc=log(predicted[i]+1)
		la=log(actual[i]+1)
		diff=lc-la
		res+=diff*diff

	res=res/len(predicted)
	return sqrt(res)

