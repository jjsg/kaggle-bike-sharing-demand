****************************Project****************************

This was online competition in kaggle where we participated in team of 2.
http://www.kaggle.com/c/bike-sharing-demand


****************************What we did?**************************

In short:
1. Splitted and added few features as suitable (time,weekend,binary season)
2. Individually predicted casual and registered and then added for result
3. We used separate Regression Algorithm for casual and registered, RandomForestRegressor and ExtraTreeRegressor respectively
4. Instead of predicting count we predicted its logarithmic as it decreases the error to great extend


*******************************Details**************************

It consists of following files:
1. /bin 
	Run the bash file which will include all the required files
2. /data
	It consists of both train and test data provided by the kaggle
3. /result
	It consists of a output file which contains test data with its predicted count
4. /src
	It consists of source code
5. Also it contains presentation and the entire report of out project (odt and pdf)
