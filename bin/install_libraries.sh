#!/bin/bash


#Run this file to install the language and its corresponding libraries reqd to run the code
sudo apt-get install python2.7,python-pip
sudo pip install numpy
sudo pip install pandas
sudo pip install scikit-learn
